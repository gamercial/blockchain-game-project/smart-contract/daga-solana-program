import { PublicKey } from "@solana/web3.js";
import { establishConnection, establishPayer } from "./contexts";
import {
  checkProgram,
  createInventorySlot,
  createTrackerAccount,
  getInventorySlot,
  getTokenProperty,
  getTracker,
  mint
} from "./egg";


async function main() {
  console.log("Let's say hello to a Solana account...");

  // Establish connection to the cluster
  await establishConnection();

  // Determine who pays for the fees
  await establishPayer();

  // Check if the program has been deployed
  await checkProgram();

  const trackerPubKey = await createTrackerAccount();
  console.log(`Created Tracker: ${trackerPubKey}`)

  const inventorySlotPubKey = await createInventorySlot();
  console.log(`Created InventorySlot: ${inventorySlotPubKey}`)

  const tokenAddress = await mint('Daga Solana NFT', trackerPubKey, inventorySlotPubKey);
  console.log(`Created Token: ${tokenAddress}`)

  console.log(` ===== READING ===== `)
  await getTokenProperty(tokenAddress.toBase58());
  await getTracker(trackerPubKey.toBase58());
  await getInventorySlot(inventorySlotPubKey.toBase58());

  console.log('Success');
}

main().then(
  () => process.exit(),
  err => {
    console.error(err);
    process.exit(-1);
  },
);
