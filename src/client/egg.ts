/* eslint-disable */
import {
  PublicKey,
  sendAndConfirmTransaction,
  SystemProgram,
  Transaction,
  TransactionInstruction,
} from '@solana/web3.js';
import fs from 'mz/fs';
import path from 'path';
import {createKeypairFromFile, makeDString} from './utils';
import {connection, payer} from './contexts';
// @ts-ignore
import * as BufferLayout from 'buffer-layout';
import {EggMetadataLayout, InventorySlotLayout} from './layout';
import {Buffer} from "buffer";
import {EggLayout} from "./layout/eggLayout";
import { TrackerLayout } from './layout/trackerLayout';

/**
 * Solana NFT's program id
 */
let programId: PublicKey;

/**
 * Path to program files
 */
const PROGRAM_PATH = path.resolve(__dirname, '../../dist/program');

/**
 * Path to program shared object file which should be deployed on chain.
 * This file is created when running either:
 *   - `npm run build:program-c`
 *   - `npm run build:program-rust`
 */
const PROGRAM_SO_PATH = path.join(PROGRAM_PATH, 'solana_nft.so');

/**
 * Path to the keypair of the deployed program.
 * This file is created when running `solana program deploy dist/program/solana_nft.so`
 */
const PROGRAM_KEYPAIR_PATH = path.join(PROGRAM_PATH, 'solana_nft-keypair.json');

/**
 * Check if the hello world BPF program has been deployed
 */
export async function checkProgram(): Promise<void> {
  // Read program id from keypair file
  try {
    const programKeypair = await createKeypairFromFile(PROGRAM_KEYPAIR_PATH);
    programId = programKeypair.publicKey;
  } catch (err) {
    const errMsg = (err as Error).message;
    throw new Error(
      `Failed to read program keypair at '${PROGRAM_KEYPAIR_PATH}' due to error: ${errMsg}. Program may need to be deployed with \`solana program deploy dist/program/helloworld.so\``,
    );
  }

  // Check if the program has been deployed
  const programInfo = await connection.getAccountInfo(programId);
  if (programInfo === null) {
    if (fs.existsSync(PROGRAM_SO_PATH)) {
      throw new Error(
        'Program needs to be deployed with `solana program deploy dist/program/helloworld.so`',
      );
    } else {
      throw new Error('Program needs to be built and deployed');
    }
  } else if (!programInfo.executable) {
    throw new Error(`Program is not executable`);
  }
  console.log(`Using program ${programId.toBase58()}`);
}

export async function createTrackerAccount() {
//  const DATA_SLOT_SEED = payer.publicKey.toBase58();
  const DATA_SLOT_SEED = 'tracker_account____' + Date.now().toString();

  const newStorageSlotPubKey = await PublicKey.createWithSeed(
    payer.publicKey,
    DATA_SLOT_SEED,
    programId,
  );
  console.log("user tracker account", newStorageSlotPubKey.toBase58())

  /**
   * The expected size of the tracker storage.
   */

  const STORAGE_SIZE = TrackerLayout().span + 1;
  const lamports = await connection.getMinimumBalanceForRentExemption(
    STORAGE_SIZE,
  );

  const transaction = new Transaction().add(
    SystemProgram.createAccountWithSeed({
      fromPubkey: payer.publicKey,
      basePubkey: payer.publicKey,
      seed: DATA_SLOT_SEED,
      newAccountPubkey: newStorageSlotPubKey,
      lamports,
      space: STORAGE_SIZE,
      programId,
    }),
  );
  await sendAndConfirmTransaction(connection, transaction, [payer]);

  const dataLayout = BufferLayout.struct([
    BufferLayout.u8('instruction'),
  ]);

  const dto = {
    instruction: 1, // instruction
  }
  let data = Buffer.alloc(1024);
  const encodeLength = dataLayout.encode(dto, data);
  data = data.slice(0, encodeLength);

  const instruction = new TransactionInstruction({
    keys: [
        {pubkey: payer.publicKey, isSigner: true, isWritable: false},
        {pubkey: newStorageSlotPubKey, isSigner: false, isWritable: true}
    ],
    programId,
    data,
  });

  await sendAndConfirmTransaction(
    connection,
    new Transaction().add(instruction),
    [payer],
  );

  return newStorageSlotPubKey
}

export async function createInventorySlot() {
  const DATA_SLOT_SEED = 'inventory_account__' + Date.now().toString();

  const newInventorySlotPubKey = await PublicKey.createWithSeed(
      payer.publicKey,
      DATA_SLOT_SEED,
      programId,
  );

  const INVENTORY_SLOT_SIZE = InventorySlotLayout().span + 1;
  const lamports = await connection.getMinimumBalanceForRentExemption(
      INVENTORY_SLOT_SIZE,
  );
  const transaction = new Transaction().add(
      SystemProgram.createAccountWithSeed({
        fromPubkey: payer.publicKey,
        basePubkey: payer.publicKey,
        seed: DATA_SLOT_SEED,
        newAccountPubkey: newInventorySlotPubKey,
        lamports,
        space: INVENTORY_SLOT_SIZE,
        programId,
      }),
  );
  await sendAndConfirmTransaction(connection, transaction, [payer]);

  const dataLayout = BufferLayout.struct([
    BufferLayout.u8('instruction'),
  ]);

  const dto = {
    instruction: 2, // create inventory slot instruction
  }

  let data = Buffer.alloc(1024);
  const encodeLength = dataLayout.encode(dto, data);
  data = data.slice(0, encodeLength);

  const instruction = new TransactionInstruction({
    keys: [
      {pubkey: payer.publicKey, isSigner: true, isWritable: false},
      {pubkey: newInventorySlotPubKey, isSigner: false, isWritable: true}
    ],
    programId,
    data,
  });

  await sendAndConfirmTransaction(
      connection,
      new Transaction().add(instruction),
      [payer],
  );

  return newInventorySlotPubKey
}

export async function mint(hash: string, storageKey: PublicKey, inventorySlotKey: PublicKey): Promise<PublicKey> {
  const DATA_SLOT_SEED = Date.now().toString();
  const newDataSlotPubKey = await PublicKey.createWithSeed(
    payer.publicKey,
    DATA_SLOT_SEED,
    programId,
  );
  /**
   * The expected size of the NFT.
   */

  const NFT_SIZE = EggLayout().span + 1; // Sol using first byte to recognize used slot
  const lamports = await connection.getMinimumBalanceForRentExemption(
    NFT_SIZE,
  );


  const transaction = new Transaction().add(
    SystemProgram.createAccountWithSeed({
      fromPubkey: payer.publicKey,
      basePubkey: payer.publicKey,
      seed: DATA_SLOT_SEED,
      newAccountPubkey: newDataSlotPubKey,
      lamports,
      space: NFT_SIZE,
      programId,
    }),
  );
  await sendAndConfirmTransaction(connection, transaction, [payer]);

  const dataLayout = BufferLayout.struct([
    BufferLayout.u8('instruction'),
    EggMetadataLayout('metadata')
  ]);

  const dto = {
    instruction: 0, // instruction
    metadata: {
      ownerAddress: payer.publicKey.toBuffer(),
      description: Buffer.from(makeDString(hash), 'utf-8')
    }
  }

  let data = Buffer.alloc(1024);
  {
    const encodeLength = dataLayout.encode(dto, data);
    data = data.slice(0, encodeLength)
  }

  const instruction = new TransactionInstruction({
    keys: [
      {pubkey: payer.publicKey, isSigner: true, isWritable: false},
      {pubkey: storageKey, isSigner: false, isWritable: true},
      {pubkey: inventorySlotKey, isSigner: false, isWritable: true},
      {pubkey: newDataSlotPubKey, isSigner: false, isWritable: true}
    ],
    programId,
    data,
  });

  await sendAndConfirmTransaction(
    connection,
    new Transaction().add(instruction),
    [payer],
  );

  return newDataSlotPubKey
}

export async function getTokenProperty(public_key: string) {
  const accountInfo = await connection.getAccountInfo(new PublicKey(public_key));
  if (accountInfo?.data) {
    const data = accountInfo.data.slice(1, accountInfo.data.length) // Ignore first byte for Sol logic purpose
    const tokenLayout = EggLayout();
    const decodedData = tokenLayout.decode(data);
    const decodedPublicKey = new PublicKey(decodedData.slot_key).toBase58();

    const readableData = {
      slotKey: decodedPublicKey,
      metadata: {
        ownerAddress: new PublicKey(decodedData.metadata.ownerAddress).toBase58(),
        description: decodedData.metadata.description.toString()
      },
      attribute: {
        randSeed: decodedData.attribute.randSeed.toString()
      },
      isInitialized: !!decodedData.isInitialized
    }
    console.log('TokenData: ', readableData)
    return readableData
  }
  return null
}

export async function getTracker(public_key: string) {
  const accountInfo = await connection.getAccountInfo(new PublicKey(public_key));
  if (accountInfo?.data) {
    const data = accountInfo.data.slice(1, accountInfo.data.length) // Ignore first byte for Sol logic purpose
    const trackerLayout = TrackerLayout();
    const decodedData = trackerLayout.decode(data);
    const decodedPublicKey = new PublicKey(decodedData.slot_key).toBase58();
    const readableData = {
      slotKey: decodedPublicKey,
      ownerAddress: new PublicKey(decodedData.owner_address).toBase58(),
      token_amount: 1,
      isInitialized: !!decodedData.isInitialized
    }
    console.log('TrackerData: ', readableData)
    return readableData
  }
  return null
}

export async function getInventorySlot(public_key: string) {
  const inventorySlotInfo = await connection.getAccountInfo(new PublicKey(public_key));
  if (inventorySlotInfo?.data) {
    const data = inventorySlotInfo.data.slice(1, inventorySlotInfo.data.length)

    const inventorySlotLayout = InventorySlotLayout();
    const decodedData = inventorySlotLayout.decode(data);

    const readableData = {
      slotKey: new PublicKey(decodedData.slot_key).toBase58(),
      ownerAddress: new PublicKey(decodedData.owner_address).toBase58(),
      tokenAddress: new PublicKey(decodedData.token_address).toBase58(),
      isInitialized: !!decodedData.isInitialized
    }
    console.log('InventorySlot: ', readableData)
    return readableData
  }
}