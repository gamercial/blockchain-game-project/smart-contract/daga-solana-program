/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import * as BufferLayout from 'buffer-layout';
import {publicKey, uint64} from './common';

export const TrackerLayout = (property = 'tracker') => {
    const layout = BufferLayout.struct([
        publicKey("slot_key"),
        publicKey("owner_address"),
        uint64("token_amount"),
        BufferLayout.u8("isInitialized")
    ], property)
    const maxDataTypeSpan = uint64().span
    const pureLayoutSpan = layout.span
    // CPU padding primitive types
    layout.span = Math.ceil(pureLayoutSpan/maxDataTypeSpan) * maxDataTypeSpan
    return layout
}