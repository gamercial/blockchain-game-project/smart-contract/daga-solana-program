import * as BufferLayout from 'buffer-layout';
import {publicKey} from "./common";

export const EggMetadataLayout = (property = 'string') => {
    return BufferLayout.struct([
        BufferLayout.blob(32, 'ownerAddress'),
        BufferLayout.blob(32, 'description')
    ], property)
}

export const EggAttributeLayout = (property = 'string') => {
    return BufferLayout.struct([
        BufferLayout.blob(32, 'randSeed')
    ], property)
}

export const EggLayout = (property = 'string') => {
    return BufferLayout.struct([
        publicKey("slot_key"),
        EggMetadataLayout("metadata"),
        EggAttributeLayout("attribute"),
        BufferLayout.u8("isInitialized")
    ], property)
}