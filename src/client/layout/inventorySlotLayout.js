import * as BufferLayout from 'buffer-layout';
import {publicKey} from "./common";

export const InventorySlotLayout = (property = 'string') => {
    return BufferLayout.struct([
        publicKey("slot_key"),
        publicKey("owner_address"),
        publicKey("token_address"),
        BufferLayout.u8("isInitialized"),
    ], property)
}
