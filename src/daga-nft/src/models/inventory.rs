use std::borrow::Borrow;
use solana_program::{
    pubkey::Pubkey,
    program_pack::{IsInitialized, Pack, Sealed},
    program_error::ProgramError, msg
};
use crate::utils::string::DString;
use std::mem::size_of;

// region Inventory Slot ( NFT Holder )

#[derive(Clone, Debug, Default, PartialEq)]
pub struct InventorySlot {
    pub slot_key: Pubkey,
    pub owner_address: Pubkey,
    pub token_address: Pubkey,
    pub is_initialized: bool
}

impl IsInitialized for InventorySlot {
    fn is_initialized(&self) -> bool {
        self.is_initialized
    }
}

impl Sealed for InventorySlot {}

impl Pack for InventorySlot {
    const LEN: usize = size_of::<u8>() + size_of::<InventorySlot>();

    fn pack_into_slice(&self, output: &mut [u8]) {
        // Use first byte to define account data already borrowed, otherwise it will panic
        output[0] = 3;
        #[allow(clippy::cast_ptr_alignment)]
            let value = unsafe { &mut *(&mut output[1] as *mut u8 as *mut InventorySlot) };
        *value = self.clone();
    }

    fn unpack_from_slice(src: &[u8]) -> Result<Self, ProgramError> {
        #[allow(clippy::cast_ptr_alignment)]
            let token: &InventorySlot = unsafe { &*(&src[1] as *const u8 as *const InventorySlot) };
        Ok(token.clone())
    }
}

// endregion