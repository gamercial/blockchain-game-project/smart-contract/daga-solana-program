use std::borrow::Borrow;
use solana_program::{
    pubkey::Pubkey,
    program_pack::{IsInitialized, Pack, Sealed},
    program_error::ProgramError, msg
};
use crate::utils::string::DString;
use std::mem::size_of;
use arrayref::{array_ref, array_refs, mut_array_refs, array_mut_ref};


// region Storage

#[derive(Clone, Debug, PartialEq)]
pub struct Storage {
    pub slot_key: Pubkey,
    pub owner_address: Pubkey,
    pub token_amount: u64,
    pub is_initialized: bool
}

impl Default for Storage {
    fn default() -> Self {
        Storage {
            slot_key: Pubkey::default(),
            owner_address: Pubkey::default(),
            token_amount: 0u64,
            is_initialized: false
        }
    }
}

impl IsInitialized for Storage {
    fn is_initialized(&self) -> bool {
        self.is_initialized
    }
}

impl Sealed for Storage {}

impl Pack for Storage {
    const LEN: usize = size_of::<u8>() + size_of::<Storage>();

    fn pack_into_slice(&self, output: &mut [u8]) {
        let output = array_mut_ref![output, 0, Storage::LEN];
        let (
            data_index,
            slot_key_ref,
            owner_key_ref,
            amount_ref,
            is_init_ref
        ) = mut_array_refs![output, 1, 32, 32, 8, 8];

        let &Storage {
            ref slot_key,
            ref owner_address,
            token_amount,
            is_initialized
        } = self;

        data_index[0] = 2u8;
        slot_key_ref.copy_from_slice(slot_key.as_ref());
        owner_key_ref.copy_from_slice(owner_address.as_ref());
        amount_ref.copy_from_slice(unsafe {std::slice::from_raw_parts((&token_amount as *const u64) as *const u8, 8)});
        is_init_ref[0] = is_initialized as u8;
    }

    fn unpack_from_slice(src: &[u8]) -> Result<Self, ProgramError> {
        let src = array_ref![src, 0, Storage::LEN];
        let (
            data_index,
            slot_key_ref,
            owner_key_ref,
            amount_ref,
            over_padding
        ) = array_refs![src, 1, 32, 32, 8, 8];
        Ok(
            Storage {
                slot_key: Pubkey::new(slot_key_ref),
                owner_address: Pubkey::new(owner_key_ref),
                token_amount: u64::from_le_bytes(amount_ref.clone()),
                is_initialized: over_padding[0] != 0
            }
        )
    }
}

// endregion