use std::borrow::Borrow;
use solana_program::{
    pubkey::Pubkey,
    program_pack::{IsInitialized, Pack, Sealed},
    program_error::ProgramError
};
use crate::utils::string::DString;
use std::mem::size_of;

// region Egg Token

#[derive(Clone, Debug, Default, PartialEq)]
pub struct EggMetadata {
    pub owner_address: Pubkey,
    pub description: DString
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct EggAttribute {
    pub rad_seed: DString
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Egg {
    pub slot_key: Pubkey,
    pub meta_data: EggMetadata,
    pub attribute: EggAttribute,
    pub is_initialized: bool,
}

impl IsInitialized for Egg {
    fn is_initialized(&self) -> bool {
        self.is_initialized
    }
}

impl Sealed for Egg {}

impl Pack for Egg {
    const LEN: usize = size_of::<u8>() + size_of::<Egg>();

    fn pack_into_slice(&self, output: &mut [u8]) {
        // Use first byte to define account data already borrowed, otherwise it will panic
        output[0] = 1;
        #[allow(clippy::cast_ptr_alignment)]
            let value = unsafe { &mut *(&mut output[1] as *mut u8 as *mut Egg) };
        *value = self.clone();
    }

    fn unpack_from_slice(src: &[u8]) -> Result<Self, ProgramError> {
        #[allow(clippy::cast_ptr_alignment)]
            let token: &Egg = unsafe { &*(&src[1] as *const u8 as *const Egg) };
        Ok(token.clone())
    }
}

// endregion