use std::fmt;
use std::fmt::Formatter;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct DString([u8; 32]);

impl DString {
    #[allow(dead_code)]
    pub fn to_string(self: &Self) -> String {
        let str = match std::str::from_utf8(&self.0) {
            Ok(s) => s,
            _ => panic!("Invalid DString data to_string")
        };
        str.to_string()
    }

    pub fn from(string: String) -> DString {
        let char_vec: Vec<char> = string.chars().collect();
        let mut val = [32u8; 32];
        for x in 0..31 {
            if x < char_vec.len() {
                val[x] = char_vec[x] as u8;
            } else {
                val[x] = 32u8; // Blank character
            }
        }
        DString(val)
    }
}

impl fmt::Display for DString {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let t = match std::str::from_utf8(&self.0) {
            Ok(v) => v,
            _ => " "
        };
        write!(f, "DString({:?})", t.to_string())
    }
}

