use std::borrow::Borrow;
use crate::{
    models::egg::{Egg, EggMetadata, EggAttribute},
    models::storage::Storage,
    models::inventory::InventorySlot,
    utils::string::DString
};
use std::mem::size_of;
use solana_program::{
    account_info::{next_account_info, AccountInfo},
    entrypoint::ProgramResult,
    program_error::ProgramError,
    program_pack::{Pack},
    pubkey::Pubkey,
    msg,
};

#[allow(dead_code)]
unsafe fn any_as_u8_slice<T: Sized>(p: &T) -> &[u8] {
    ::std::slice::from_raw_parts(
        (p as *const T) as *const u8,
        ::std::mem::size_of::<T>(),
    )
}

#[repr(C)]
#[derive(PartialEq)]
pub enum Processor {
    Unallocated,
    Token(Egg),
    AccountStorage(Storage),
    AccountInventorySlot(InventorySlot),
}

impl Processor {
    /// Processes an [Instruction](enum.Instruction.html).
    pub fn process(program_id: &Pubkey, accounts: &[AccountInfo], input: &[u8]) -> ProgramResult {
        let command = Command::deserialize(input)?;

        match command {
            Command::Mint(metadata) => {
                Self::process_mint(&program_id, &accounts, metadata)
            }
            Command::InspectToken => {
                Self::process_unpack_token(&program_id, &accounts)
            }
            Command::NewStorage => {
                Self::process_new_storage(&program_id, &accounts)
            }
            Command::InspectStorage => {
                Self::process_inspect_storage(&program_id, &accounts)
            }
            Command::NewInventorySlot => {
                Self::process_new_inventory_slot(&program_id, &accounts)
            }
            Command::InspectInventorySlot => {
                Self::process_inspect_inventory_slot(&program_id, &accounts)
            }
        }
    }

    pub fn process_mint(program_id: &Pubkey, accounts: &[AccountInfo], metadata: EggMetadata) -> ProgramResult {
        let account_info_iter = &mut accounts.iter();
        let owner_account = next_account_info(account_info_iter)?;
        let storage_account = next_account_info(account_info_iter)?;
        let inventory_slot_account = next_account_info(account_info_iter)?;
        let account = next_account_info(account_info_iter)?;

        if account.owner != program_id {
            msg!("Error: Account is not owned by this program");
            return Err(ProgramError::IncorrectProgramId);
        }

        // TODO: Only storage owner can mint
        // TODO: Need to find out what is signer
        // if !account.is_signer {
        //     msg!("Error: Only signer can mint")
        // }
        if !owner_account.is_signer {
            msg!("Error: Owner account must be transaction signer");
            return Err(ProgramError::InvalidAccountData);
        }

        let mut storage_data = storage_account.data.borrow_mut();
        let mut inventory_slot_data = inventory_slot_account.data.borrow_mut();

        // region Verify storage is valid
        if let Processor::AccountStorage(storage) = Processor::deserialize(&storage_data)? {
            if !storage.is_initialized {
                msg!("Error: Storage has not initialized.");
                return Err(ProgramError::InvalidAccountData)
            }
        } else {
            msg!("Error: Account is not a Storage or has not been initialized");
            return Err(ProgramError::InvalidArgument);
        }
        // endregion

        // region Verify inventory slot is valid and empty
        if let Processor::AccountInventorySlot(inventory_slot) = Processor::deserialize(&inventory_slot_data)? {
            if !(inventory_slot.is_initialized) {
                msg!("Error: Inventory slot has not initialized.");
                return Err(ProgramError::InvalidAccountData)
            }
            if !(inventory_slot.token_address.to_bytes() == Pubkey::default().to_bytes()) {
                msg!("Error: Inventory slot is not empty");
                return Err(ProgramError::InvalidAccountData)
            }
        } else {
            msg!("Error: Account is not a Inventory slot or has not been initialized");
            return Err(ProgramError::InvalidArgument);
        }
        // endregion

        let mut account_data = account.data.borrow_mut();
        if Processor::Unallocated != Processor::deserialize(account_data.borrow())? {
            msg!("Error: Token is already allocated");
            return Err(ProgramError::InvalidArgument);
        }

        // TODO: Random token attribute as mint behavior?
        // Currently there is nothing randomness in Solana
        let egg_attr = EggAttribute {rad_seed: DString::from("__something_to_random__".to_string())};
        let mut egg_metadata = metadata.clone();
        egg_metadata.owner_address = *owner_account.key;
        let egg = Egg {
            slot_key: *account.key,
            meta_data: egg_metadata,
            attribute: egg_attr,
            is_initialized: true
        };
        egg.pack_into_slice(&mut account_data);

        let mut inventory_slot: InventorySlot = InventorySlot::unpack_from_slice(&mut inventory_slot_data).unwrap();
        inventory_slot.token_address = *account.key;
        inventory_slot.pack_into_slice(&mut inventory_slot_data);

        let mut storage: Storage = Storage::unpack_from_slice(&mut storage_data).unwrap();
        storage.token_amount = storage.token_amount + 1;
        storage.pack_into_slice(&mut storage_data);

        Ok(())
    }

    pub fn process_new_storage(program_id: &Pubkey, accounts: &[AccountInfo]) -> ProgramResult {
        let account_info_iter = &mut accounts.iter();
        let owner_account = next_account_info(account_info_iter)?;
        let storage_account = next_account_info(account_info_iter)?;

        if storage_account.owner != program_id {
            msg!("Error: Account is not owned by this program");
            return Err(ProgramError::IncorrectProgramId);
        }

        let mut account_data = storage_account.data.borrow_mut();
        if Processor::Unallocated != Processor::deserialize(account_data.borrow())? {
            msg!("Error: Storage is already allocated");
            return Err(ProgramError::InvalidArgument);
        }

        let empty_storage = Storage {
            slot_key: *storage_account.key,
            owner_address: *owner_account.key,
            token_amount: 0u64,
            is_initialized: true
        };
        empty_storage.pack_into_slice(&mut account_data);
        Ok(())
    }

    pub fn process_new_inventory_slot(program_id: &Pubkey, accounts: &[AccountInfo]) -> ProgramResult {
        let account_info_iter = &mut accounts.iter();
        let owner_account = next_account_info(account_info_iter)?;
        let inventory_slot_account = next_account_info(account_info_iter)?;

        if inventory_slot_account.owner != program_id {
            msg!("Error: Account is not owned by this program");
            return Err(ProgramError::IncorrectProgramId);
        }

        let mut account_data = inventory_slot_account.data.borrow_mut();
        if Processor::Unallocated != Processor::deserialize(account_data.borrow())? {
            msg!("Error: Storage is already allocated");
            return Err(ProgramError::InvalidArgument);
        }

        let empty_inventory_slot = InventorySlot {
            slot_key: *inventory_slot_account.key,
            owner_address: *owner_account.key,
            token_address: Pubkey::default(),
            is_initialized: true
        };
        empty_inventory_slot.pack_into_slice(&mut account_data);
        Ok(())
    }

    // region Internal functions
    pub fn process_unpack_token(program_id: &Pubkey, accounts: &[AccountInfo]) -> ProgramResult {
        let account_info_iter = &mut accounts.iter();
        let account = next_account_info(account_info_iter)?;
        if account.owner != program_id {
            msg!("Error: Account is not owned by this program");
            return Err(ProgramError::IncorrectProgramId);
        }

        let mut account_data = account.data.borrow_mut();

        msg!("Token result: {:?}", Egg::unpack_from_slice(&mut account_data));
        Ok(())
    }

    pub fn process_inspect_storage(program_id: &Pubkey, accounts: &[AccountInfo]) -> ProgramResult {
        let account_info_iter = &mut accounts.iter();
        let account = next_account_info(account_info_iter)?;
        if account.owner != program_id {
            msg!("Error: Account is not owned by this program");
            return Err(ProgramError::IncorrectProgramId);
        }

        let mut account_data = account.data.borrow_mut();

        let storage = Storage::unpack_from_slice(&mut account_data).unwrap();
        msg!("Token result: Amount::{:?} Owner::{:?}", storage.token_amount, storage.owner_address);
        Ok(())
    }

    pub fn process_inspect_inventory_slot(program_id: &Pubkey, accounts: &[AccountInfo]) -> ProgramResult {
        let account_info_iter = &mut accounts.iter();
        let account = next_account_info(account_info_iter)?;
        if account.owner != program_id {
            msg!("Error: Account is not owned by this program");
            return Err(ProgramError::IncorrectProgramId);
        }

        let mut account_data = account.data.borrow_mut();

        let inventory_slot = InventorySlot::unpack_from_slice(&mut account_data).unwrap();
        msg!("Inventory result: holding token: {:?}", inventory_slot.token_address);
        Ok(())
    }

    // endregion

    // region Processor Serialization
    fn deserialize(input: &[u8]) -> Result<Self, ProgramError> {

        if input.len() < std::mem::size_of::<u8>() {
            return Err(ProgramError::InvalidAccountData);
        }
        Ok(match input[0] {
            0 => Self::Unallocated,
            1 => {
                if input.len() < size_of::<u8>() + size_of::<Egg>() {
                    msg!("Error: Invalid token account length");
                    return Err(ProgramError::InvalidAccountData);
                }

                #[allow(clippy::cast_ptr_alignment)]
                    let egg: &Egg = unsafe { &*(&input[1] as *const u8 as *const Egg) };
                Self::Token(egg.clone())
            }
            2 => {
                if input.len() < size_of::<u8>() + size_of::<Storage>() {
                    msg!("Error: Invalid storage account length");
                    return Err(ProgramError::InvalidAccountData)
                }
                let storage = Storage::unpack_from_slice(input).unwrap();
                Self::AccountStorage(storage)
            }
            3 => {
                if input.len() < size_of::<u8>() + size_of::<InventorySlot>() {
                    msg!("Error: Invalid inventory slot length");
                    return Err(ProgramError::InvalidAccountData)
                }
                #[allow(clippy::cast_ptr_alignment)]
                    let inventory: &InventorySlot = unsafe { &*(&input[1] as *const u8 as *const InventorySlot) };
                Self::AccountInventorySlot(inventory.clone())
            }
            _ => {
                msg!("Error: Cannot deserializer account data");
                return Err(ProgramError::InvalidAccountData)
            }
        })
    }

    // endregion
}

#[repr(C)]
#[derive(Clone, Debug, PartialEq)]
pub enum Command {
    Mint(EggMetadata), // 0
    NewStorage, // 1
    NewInventorySlot, // 2
    InspectToken, // 100 Internal test only
    InspectStorage, // 101 Internal test only
    InspectInventorySlot, // 102 Internal test only
}

impl Command {
    pub fn deserialize(input: &[u8]) -> Result<Self, ProgramError> {
        if input.len() < size_of::<u8>() {
            return Err(ProgramError::InvalidAccountData);
        }
        Ok(match input[0] {
            0 => {
                if input.len() < size_of::<u8>() + size_of::<EggMetadata>() {
                    return Err(ProgramError::InvalidAccountData);
                }
                #[allow(clippy::cast_ptr_alignment)]
                    let token: &EggMetadata = unsafe { &*(&input[1] as *const u8 as *const EggMetadata) };
                Self::Mint(token.clone())
            }
            1 => Self::NewStorage,
            2 => Self::NewInventorySlot,
            100 => Self::InspectToken,
            101 => Self::InspectStorage,
            102 => Self::InspectInventorySlot,
            _ => return Err(ProgramError::InvalidAccountData),
        })
    }

    #[allow(dead_code)]
    pub fn serialize(self: &Self, output: &mut [u8]) -> ProgramResult {
        if output.len() < size_of::<u8>() {
            return Err(ProgramError::InvalidAccountData);
        }
        match self {
            Self::Mint(token) => {
                if output.len() < size_of::<u8>() + size_of::<EggMetadata>() {
                    return Err(ProgramError::InvalidInstructionData);
                }
                output[0] = 0;
                #[allow(clippy::cast_ptr_alignment)]
                    let value = unsafe { &mut *(&mut output[1] as *mut u8 as *mut EggMetadata) };
                *value = token.clone();
            }
            Self::NewStorage => {
                output[0] = 1
            }
            Self::NewInventorySlot => {
                output[0] = 2
            }
            Self::InspectToken => {
                output[0] = 100;
            }
            Self::InspectStorage => {
                output[0] = 101;
            }
            Self::InspectInventorySlot => {
                output[0] = 102;
            }
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use solana_program::clock::Epoch;

    const METADATA_LEN: usize = size_of::<u8>() + size_of::<Egg>();
    const STORAGE_LEN: usize = size_of::<u8>() + size_of::<Storage>();
    const INVENTORY_SLOT_LEN: usize = size_of::<u8>() + size_of::<InventorySlot>();

    fn new_pubkey(id: u8) -> Pubkey {
        Pubkey::new(&vec![
            id, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1,
        ])
    }

    #[test]
    fn test_new_storage() {
        let program_id: Pubkey = new_pubkey(1);

        // region PayerAccount
        let payer_account = new_pubkey(2);
        let mut lamports = 0u64;
        let mut payer_data = vec![0u8; STORAGE_LEN];

        let payer = AccountInfo::new(
            &payer_account,
            true,
            true,
            &mut lamports,
            &mut payer_data,
            &program_id,
            false,
            Epoch::default()
        );
        // endregion
        // region StorageSlotAccount
        let storage_account = new_pubkey(3);
        let mut storage_lamports = 0u64;
        let mut storage_data = vec![0u8; STORAGE_LEN];

        let storage = AccountInfo::new(
            &storage_account,
            false,
            true,
            &mut storage_lamports,
            &mut storage_data,
            &program_id,
            false,
            Epoch::default()
        );
        // endregion
        let accounts = vec![payer, storage];

        let mut instruction_data = vec![0u8; size_of::<Command>()];
        Command::NewStorage.serialize(&mut instruction_data).unwrap();

        // Test create egg token with metadata
        assert_eq!(Processor::process(&program_id, &accounts, &instruction_data), Ok(()));
        // Test mint egg token to already allocated account
        assert_eq!(Processor::process(&program_id, &accounts, &instruction_data), Err(ProgramError::InvalidArgument));

        // Inspect storage
        // let mut instruction_inspect = Command::InspectStorage;
        // let mut instruction_inspect_data = vec![0u8; size_of::<u8>()];
        // instruction_inspect.serialize(&mut instruction_inspect_data);
        // Processor::process(&program_id, &[storage.to_owned()], &instruction_inspect_data);
    }

    #[test]
    fn test_new_inventory_slot() {
        let program_id: Pubkey = new_pubkey(1);

        // region PayerAccount
        let payer_account = new_pubkey(2);
        let mut lamports = 0u64;
        let mut payer_data = vec![0u8; STORAGE_LEN];

        let payer = AccountInfo::new(
            &payer_account,
            true,
            true,
            &mut lamports,
            &mut payer_data,
            &program_id,
            false,
            Epoch::default()
        );
        // endregion
        // region InventorySlotAccount
        let inventory_slot_account = new_pubkey(3);
        let mut inventory_slot_lamports = 0u64;
        let mut inventory_slot_data = vec![0u8; INVENTORY_SLOT_LEN];

        let inventory_slot = AccountInfo::new(
            &inventory_slot_account,
            false,
            true,
            &mut inventory_slot_lamports,
            &mut inventory_slot_data,
            &program_id,
            false,
            Epoch::default()
        );
        // endregion
        let accounts = vec![payer, inventory_slot];

        let mut instruction_data = vec![0u8; size_of::<Command>()];
        Command::NewInventorySlot.serialize(&mut instruction_data).unwrap();

        // Test create inventory slot
        assert_eq!(Processor::process(&program_id, &accounts, &instruction_data), Ok(()));
        // Test create inventory slot to allocated account
        assert_eq!(Processor::process(&program_id, &accounts, &instruction_data), Err(ProgramError::InvalidArgument));

        // let mut instruction_inspect = Command::InspectStorage;
        // let mut instruction_inspect_data = vec![0u8; size_of::<u8>()];
        // instruction_inspect.serialize(&mut instruction_inspect_data);
        // Processor::process(&program_id, &accounts, &instruction_inspect_data);
    }

    #[test]
    fn test_mint_egg() {
        let program_id = new_pubkey(1);
        // region PayerAccount
        let payer_account = new_pubkey(2);
        let mut lamports = 0u64;
        let mut payer_data = vec![0u8; STORAGE_LEN];

        let payer = AccountInfo::new(
            &payer_account,
            true,
            true,
            &mut lamports,
            &mut payer_data,
            &program_id,
            false,
            Epoch::default()
        );
        // endregion
        // region StorageSlotAccount
        let storage_slot_account = new_pubkey(4);
        let mut storage_lamports = 0u64;
        let mut storage_data = vec![0u8; STORAGE_LEN];
        let storage_slot = AccountInfo::new(
            &storage_slot_account,
            true,
            true,
            &mut storage_lamports,
            &mut storage_data,
            &program_id,
            false,
            Epoch::default()
        );
        // endregion
        // region InventorySlotAccount
        let inventory_slot_account = new_pubkey(5);
        let mut inventory_slot_lamports = 0u64;
        let mut inventory_slot_data = vec![0u8; INVENTORY_SLOT_LEN];

        let inventory_slot = AccountInfo::new(
            &inventory_slot_account,
            false,
            true,
            &mut inventory_slot_lamports,
            &mut inventory_slot_data,
            &program_id,
            false,
            Epoch::default()
        );
        // endregion
        // region TokenSlotAccount
        let token_slot_account = new_pubkey(3);
        let mut lamports = 0u64;
        let mut token_data = vec![0u8; METADATA_LEN];
        let token_slot = AccountInfo::new(
            &token_slot_account,
            true,
            true,
            &mut lamports,
            &mut token_data,
            &program_id,
            false,
            Epoch::default()
        );
        // endregion

        // region CreateStorage
        let instruction_create_storage = Command::NewStorage;
        let mut instruction_create_storage_data = vec![0u8; size_of::<Command>()];
        let create_storage_accounts = vec![payer.to_owned(), storage_slot.to_owned()];
        instruction_create_storage.serialize(&mut instruction_create_storage_data).unwrap();
        Processor::process(&program_id, &create_storage_accounts, &instruction_create_storage_data).unwrap();
        // endregion
        // region CreateInventorySlot
        let instruction_create_inventory_slot = Command::NewInventorySlot;
        let mut instruction_create_inventory_slot_data = vec![0u8; size_of::<Command>()];
        let create_inventory_slot_accounts = vec![payer.to_owned(), inventory_slot.to_owned()];
        instruction_create_inventory_slot.serialize(&mut instruction_create_inventory_slot_data).unwrap();
        Processor::process(&program_id, &create_inventory_slot_accounts, &instruction_create_inventory_slot_data).unwrap();
        // endregion

        let accounts = vec![
            payer.to_owned(),
            storage_slot.to_owned(),
            inventory_slot.to_owned(),
            token_slot,
        ];

        let mut instruction_data = vec![0u8; size_of::<Command>()];
        let instruction = Command::Mint(EggMetadata {
            owner_address: token_slot_account,
            description: DString::from("PayerFirstEgg".to_string())
        });

        instruction.serialize(&mut instruction_data).unwrap();

        // Test create egg token with metadata
        assert_eq!(Processor::process(&program_id, &accounts, &instruction_data), Ok(()));
        // Test mint egg token to already holding inventory slot
        assert_eq!(Processor::process(&program_id, &accounts, &instruction_data), Err(ProgramError::InvalidAccountData));

        // Inspect token
        // let mut instruction_unpack = Command::InspectToken;
        // let mut instruction_unpack_data = vec![0u8; size_of::<u8>()];
        // instruction_unpack.serialize(&mut instruction_unpack_data);
        // Processor::process(&program_id, &accounts, &instruction_unpack_data);

        // Inspect storage
        // let instruction_inspect = Command::InspectStorage;
        // let mut instruction_inspect_data = vec![0u8; size_of::<u8>()];
        // instruction_inspect.serialize(&mut instruction_inspect_data).unwrap();
        // Processor::process(&program_id, &[storage_slot.to_owned()], &instruction_inspect_data).unwrap();

        // Inspect inventory slot
        // let instruction_inspect = Command::InspectInventorySlot;
        // let mut instruction_inspect_data = vec![0u8; size_of::<u8>()];
        // instruction_inspect.serialize(&mut instruction_inspect_data).unwrap();
        // Processor::process(&program_id, &[inventory_slot.to_owned()], &instruction_inspect_data).unwrap();
    }
}